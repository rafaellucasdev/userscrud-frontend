import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UsersApiService {

  readonly usersApiUrl = "https://localhost:7201/api";

  constructor(private http:HttpClient) { }

  getUserList():Observable<any[]>{
    return this.http.get<any>(this.usersApiUrl + '/users');
  }

  addUser(data:any){
    return this.http.post(this.usersApiUrl + '/users', data);
  }

  updateUser(id:number | string, data:any){
    return this.http.put(this.usersApiUrl + `/users/${id}`, data);
  }

  deleteUser(id:number | string){
    return this.http.delete(this.usersApiUrl + `/users/${id}`);
  }

  //Tipos de Usuário
  getUserTypesList():Observable<any[]>{
    return this.http.get<any>(this.usersApiUrl + '/userstypes');
  }

  addUserType(data:any){
    return this.http.post(this.usersApiUrl + '/userstypes', data);
  }

  updateUserType(id:number | string, data:any){
    return this.http.put(this.usersApiUrl + `/userstypes/${id}`, data);
  }

  deleteUserType(id:number | string){
    return this.http.delete(this.usersApiUrl + `/userstypes/${id}`);
  }

}

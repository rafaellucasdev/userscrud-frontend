import { HttpClientModule} from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { UsersComponent } from './users/users.component';
import { ShowUsersComponent } from './users/show-users/show-users.component';
import { ManageUsersComponent } from './users/manage-users/manage-users.component';
import { UsersApiService } from './users-api.service';

@NgModule({
  declarations: [
    AppComponent,
    UsersComponent,
    ShowUsersComponent,
    ManageUsersComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [UsersApiService],
  bootstrap: [AppComponent]
})
export class AppModule { }

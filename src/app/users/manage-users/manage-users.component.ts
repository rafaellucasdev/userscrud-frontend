import { Component, Input, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { UsersApiService } from 'src/app/users-api.service';
import {  FormControl, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-manage-users',
  templateUrl: './manage-users.component.html',
  styleUrls: ['./manage-users.component.css']
})
export class ManageUsersComponent implements OnInit {

  usersList$!: Observable<any[]>;
  userTypesList$!: Observable<any[]>;
  public emailUserInput: any;
  public invalid: any;
  public touched: any;
  usersForm = new FormGroup({
    email: new FormControl('', Validators.required),
    firstname: new FormControl('', Validators.required),
    lastname: new FormControl('', Validators.required),
    birthdate: new FormControl('', [Validators.required],),
    levelofschooling: new FormControl('', [Validators.required],),
  })

  get email(){
    return this.usersForm.get('email')
  }

  get firstname(){
    return this.usersForm.get('firstname')
  }

  get lastname(){
    return this.usersForm.get('lastname')
  }

  get birthdate(){
    return this.usersForm.get('birthdate')
  }

  get levelofschooling(){
    return this.usersForm.get('levelOfSchooling')
  }

  constructor(private service:UsersApiService) {}

  @Input() user:any;
  id:number = 0;
  firstName:string = "";
  lastName:string = "";
  userTypeId!:number;
  emailUser:string = "";
  birthDate:string = "";
  levelOfSchooling!:number;

  ngOnInit(): void {
    this.id = this.user.id;
    this.firstName = this.user.firstName;
    this.lastName = this.user.lastName;
    this.userTypeId = this.user.userTypeId;
    this.emailUser = this.user.emailUser;
    this.birthDate = this.user.birthDate;
    this.levelOfSchooling = this.levelOfSchooling;
    this.usersList$ = this.service.getUserList();
    this.userTypesList$ = this.service.getUserTypesList();
    
  }

  addUser(){
    
    var user = {
      firstName:this.firstName,
      lastName:this.lastName,
      userTypeId:this.userTypeId,
      emailUser:this.emailUser,
      birthDate:this.birthDate,
      levelOfSchooling:this.levelOfSchooling,
    }

    this.service.addUser(user).subscribe(res => {
      //onsole.log('Campos: ', this.userForm!.valid)
      var closeModalBtn = document.getElementById('ManageModalClose');
      if(closeModalBtn) {
        closeModalBtn.click();
        
      }

      var showAddSuccess = document.getElementById('addSuccessAlert');
      if(showAddSuccess){
        showAddSuccess.style.display = 'block';
      }
      setTimeout(function(){
        if(showAddSuccess){
          showAddSuccess.style.display = 'none';
        }
      },3000)
    })

  }

  updateUser(){
    var user = {
      id:this.id,
      firstName:this.firstName,
      lastName:this.lastName,
      userTypeId:this.userTypeId,
      emailUser:this.emailUser,
      birthDate:this.birthDate,
      levelOfSchooling:this.levelOfSchooling,
    }

    var id:number = this.id;

    this.service.updateUser(id, user).subscribe(res => {
      var closeModalBtn = document.getElementById('ManageModalClose');
      if(closeModalBtn) {
        closeModalBtn.click();
      }

      var showUpdateSuccess = document.getElementById('updateSuccessAlert');
      if(showUpdateSuccess){
        showUpdateSuccess.style.display = 'block';
      }
      setTimeout(function(){
        if(showUpdateSuccess){
          showUpdateSuccess.style.display = 'none';
        }
      },3000)
    })

  }

}

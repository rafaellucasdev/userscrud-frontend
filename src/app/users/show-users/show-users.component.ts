import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { UsersApiService } from 'src/app/users-api.service';


@Component({
  selector: 'app-show-users',
  templateUrl: './show-users.component.html',
  styleUrls: ['./show-users.component.css']
})
export class ShowUsersComponent implements OnInit {

  userList$!:Observable<any[]>;
  userTypesList$!:Observable<any[]>;
  userTypesList:any=[]

  userTypesMap:Map<number, string> = new Map();

  constructor(private service:UsersApiService) {}

  ngOnInit(): void {
    this.userList$ = this.service.getUserList();
    this.userTypesList$ = this.service.getUserTypesList();
  }

  //Tratamento do modal
  modalTitle: string = '';
  activateAddEditUserComponent:boolean = false;
  user:any;

  modalAdd(){
    this.user = {
      id: 0,
      firstName: null,
      lastName: null, 
      userTypeId: null,
      emailUser: null,
      birthDate: null,
      levelOfSchooling: null,
    },

    this.modalTitle = "Novo usuário";
    this.activateAddEditUserComponent = true;
  }

  modalEdit(item:any){
    this.user = item;
    this.modalTitle = "Edição de usuário";
    this.activateAddEditUserComponent = true;
  }

  modalClose(){
    this.activateAddEditUserComponent = false;
    this.userList$ = this.service.getUserList();
  }

  delete(item:any){
    if(confirm(`Você irá excluir o usuário ${item.firstName}`)){
      this.service.deleteUser(item.id).subscribe(res => {
        var closeModalBtn = document.getElementById('ManageModalClose');
        if(closeModalBtn) {
          closeModalBtn.click();
        }
  
        var showDeleteSuccess = document.getElementById('deleteSuccessAlert');
        if(showDeleteSuccess){
          showDeleteSuccess.style.display = 'block';
        }
        setTimeout(function(){
          if(showDeleteSuccess){
            showDeleteSuccess.style.display = 'none';
          }
        },3000)
      })
    }
  }

}
